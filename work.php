<html>
<head>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    
    <div class="container">
        <div class="alert alert-danger" role="alert">
            <font face="微軟正黑體">
                <h1 class="display-3">留言板</h1>
            </font>
        </div>   
    </div>   

</head>
<body style="background-color:#FFF0F5;">


<form method = "post" action = "index.php">

<div class="container">

<div class="container">
    <div class="form-group sm-2">
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
            <span class="badge badge-primary">
                <font face="微軟正黑體">   
                    <h4>暱稱</h4>
                </font>
            </span>
        </label>
        <input type="text" class="form-group mx-sm-9 mb-3" name="user" placeholder="username">
    </div>
    
    <div class="form-group" >
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
            <span class="badge badge-success">
                <font face="微軟正黑體">
                    <h4>標題</h4>
                </font>
            </span>
        </label>
        <input type="text" class="form-group mx-sm-9 mb-2" name="title" placeholder="請輸入標題">
    </div>
    
    <div class="form-group">
        <label for="exampleFormControlInput1" class="col-sm-0 col-form-label">
            <span class="badge badge-info">
                <font face="微軟正黑體">
                    <h4>留言內容</h4>
                </font>
            </span>
        </label>
        <br/>
        <textarea name="content"  rows="6" cols="50" >
        </textarea>
    </div>
   
    <input type="submit" class="btn btn-outline-info" name="send" value="留言"/>

    
    </div>
</form><hr/>

<?php foreach ($table as $value){ ?>
    <table style="border:3px #cccccc solid;" cellpadding="10" border='1'>
        <tr>
                <td>
                    <?php echo '暱稱'; ?>
                </td>
                <td>
                    <?php echo $value['useracount']; ?>
                </td>
        </tr>
        <tr>
                <td>
                    <?php echo '標題';?>
                </td>
                <td>
                    <?php echo $value["title"]; ?>
                </td>
        </tr>
        <tr>
                <td>
                    <?php echo '內容'; ?>
                </td>
                <td>
                <?php echo $value["content"]; ?>
                </td>
        </tr>
        <tr>
                <td>
                    <?php echo '留言時間'; ?>
                </td>
                <td>
                <?php echo $value["updatetime"]; ?>
                </td>
        </tr>
    </table>
    <hr/>               
<?php } ?>


</div>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

</body>
</html>